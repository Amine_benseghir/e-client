package com.IDP.EClient.Controller;

import com.IDP.EClient.entities.Customer;
import com.IDP.EClient.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/customer")
public class CustomerResource {

    private final CustomerService customerService;

    @Autowired
    public CustomerResource(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping("/all")
    public ResponseEntity<List<Customer>> getAllCustomer() {
        List<Customer> customers = customerService.findAllCustomer();
        return new ResponseEntity<>(customers, HttpStatus.OK);

    }

    @GetMapping(value = {"/ages/{begin}/{end}/{splitBy}","/ages/yearsList/years" })
    public ResponseEntity<List<Customer>>calcMinimumsAge(@PathVariable List<Integer> years,@PathVariable("begin")Integer begin,@PathVariable("end") Integer end,@PathVariable("splitBy") Integer splitBy){
        List<Integer> customers = customerService.calcMinimumsAge(years,begin,end,splitBy);
        return  new ResponseEntity(customers,HttpStatus.OK);

    }

    @GetMapping("/find/{id}")
    public ResponseEntity<Customer> getCustomerById(@PathVariable("id")Long id) {
        Customer customers = customerService.findCustomerById(id);
         return  new ResponseEntity<>(customers,HttpStatus.OK);
    }

    @PostMapping("/add")
    public ResponseEntity<Customer>addCustomer(@RequestBody Customer customer){
        Customer newCustomer =customerService.saveCustomer(customer);
        return  new ResponseEntity<>(newCustomer,HttpStatus.CREATED);
    }
    @PutMapping("/update")
    public ResponseEntity<Customer>updateCustomer(@RequestBody Customer customer){
        Customer updateCustomer =customerService.updateCustomer(customer);
        return new ResponseEntity<>(updateCustomer,HttpStatus.OK);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity<?>deleteCustomer(@PathVariable("id") Long id){
        customerService.deleteCustomer(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}