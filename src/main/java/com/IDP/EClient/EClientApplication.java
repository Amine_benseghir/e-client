package com.IDP.EClient;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EClientApplication {

	public static void main(String[] args) {

		SpringApplication.run(EClientApplication.class, args);
	}

}
