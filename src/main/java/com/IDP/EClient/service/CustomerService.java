package com.IDP.EClient.service;

import com.IDP.EClient.entities.Customer;
import com.IDP.EClient.exception.UserNotFoundException;
import com.IDP.EClient.repo.CustomerRepo;
import com.IDP.EClient.tool.BagRange;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class CustomerService {

    Logger logger = LoggerFactory.getLogger(CustomerService.class);

    private final CustomerRepo customerRepo;

    @Autowired
    public CustomerService(CustomerRepo customerRepo) {
        this.customerRepo = customerRepo;
    }
    public Customer saveCustomer(Customer customer){
        logger.info("customer creation");
        return customerRepo.save(customer);
    }
    public List<Customer>findAllCustomer(){
        return customerRepo.findAll();

    }
    public Customer updateCustomer(Customer customer){
        logger.info("customer updated");
        return customerRepo.save(customer);
    }

    public Customer findCustomerById(Long customerId ){
        return customerRepo.findCustomerById(customerId).orElseThrow(()->new UserNotFoundException("user by id "+customerId+"was not found"));


    }
    public void deleteCustomer(Long customerId){
        customerRepo.deleteCustomerById(customerId);
        logger.info("customer delete it");
    }

    public List<Integer> calcMinimumsAge(List<Integer> years, int begin, int end, int splitBy) {
        int nbBag = (end-begin)/splitBy - 1;

        List<BagRange> bags = new ArrayList<>();
        for(int i=0; i<=nbBag; i++) {
            bags.add(new BagRange(begin+(i*10), begin+(i*10) + 10));
        }

        for(int year : years) {
            BagRange.AddToBag(bags, year);
        }
        List<Integer> display = new ArrayList<>();

        for(int indexRange=0; indexRange<bags.size(); indexRange++) {
            for(BagRange bag: bags) {
                if(bag.years.size() > indexRange) {
                    display.add(bag.years.get(indexRange));
                }
            }
        }

        return display;
    }
}
