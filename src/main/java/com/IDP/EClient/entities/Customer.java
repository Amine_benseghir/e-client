package com.IDP.EClient.entities;



import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.util.Date;

@Entity
public class Customer implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
   @NotBlank(message = "you have to enter your first name")
    private String firstName;
    @NotBlank(message = "you have to enter your last name")
    private String lastName;
    @NotBlank(message = "you have to enter your email")
    private String email;
    @NotBlank(message = "you have to enter your phone number")
    private String phone;
    @NotBlank(message = "you have to enter your birthday")
    private String birthday;
    private String address;

    public Customer() {
    }

    public Customer(@NotBlank(message = "you have to enter your first name") String firstName, @NotBlank(message = "you have to enter your last name") String lastName, @NotBlank(message = "you have to enter your email") String email, @NotBlank(message = "you have to enter your phone number") String phone, @NotBlank(message = "you have to enter your birthday") String birthday, String address) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phone = phone;
        this.birthday = birthday;
        this.address = address;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

}
