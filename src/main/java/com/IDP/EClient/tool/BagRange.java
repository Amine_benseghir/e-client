package com.IDP.EClient.tool;


import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class BagRange {
    public BagRange(int min, int max) {
        years = new ArrayList<>();
        this.min = min;
        this.max = max;
    }

    public List<Integer> years;
    public int min, max;


    public static void AddToBag(List<BagRange> bags, int year) {
        for( BagRange bag : bags) {
            if(year >= bag.min && year <= bag.max) {
                bag.years.add(year);
                bag.years.sort(Comparator.reverseOrder());
            }
        }

    }
}
