package com.IDP.EClient;

import com.IDP.EClient.entities.Customer;
import com.IDP.EClient.repo.CustomerRepo;
import com.IDP.EClient.service.CustomerService;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.Rollback;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
public class CustomerTest {
    @Autowired
    private CustomerRepo customerRepo;


    @Test
    @Order(1)
    public void saveCustomerTest() {
        Customer customer = new Customer();
        customer.setId(1l);
        customer.setFirstName("amine");
        customer.setLastName("benseghir");
        customer.setEmail("aa@aa.aa");
        customer.setPhone("06789634");
        customer.setBirthday("12-05-1990");
        customer.setAddress("beijing china");
        customerRepo.save(customer);
        Assertions.assertThat(customer.getId()).isGreaterThan(0);

    }
    @Test
    @Order(2)
    public void findAllCustomerTest(){
        Customer customer = new Customer();
        customer.setId(1l);
        customer.setFirstName("amine");
        customer.setLastName("benseghir");
        customer.setEmail("aa@aa.aa");
        customer.setPhone("06789634");
        customer.setBirthday("12-05-1990");
        customer.setAddress("beijing china");
        customerRepo.save(customer);
        List<Customer>customers=customerRepo.findAll();
        Assertions.assertThat(customers).size().isGreaterThan(0);
    }
    @Test
    @Order(3)
   public void updateCustomerTest(){
        Customer customer = new Customer();
        customer.setId(1l);
        customer.setFirstName("amine");
        customer.setLastName("benseghir");
        customer.setEmail("aa@aa.aa");
        customer.setPhone("06789634");
        customer.setBirthday("12-05-1990");
        customer.setAddress("beijing china");
        customerRepo.save(customer);
        customer = customerRepo.findCustomerById(1L).orElse(new Customer());
       customer.setFirstName("cash");
       customerRepo.save(customer);
        Assertions.assertThat(customer.getFirstName()).isEqualTo("cash");

   }
   @Test
   @Rollback(false)
   @Order(6)
   public void deleteCustomerTest(){
        Long id = 1L;
       Customer customer = new Customer();
       customer.setId(1l);
       customer.setFirstName("amine");
       customer.setLastName("benseghir");
       customer.setEmail("aa@aa.aa");
       customer.setPhone("06789634");
       customer.setBirthday("12-05-1990");
       customer.setAddress("beijing china");
       customerRepo.save(customer);
        boolean isExistBeforeDelete = customerRepo.findCustomerById(id).isPresent();
        customerRepo.deleteCustomerById(id);
        boolean notExistAfterDelete = customerRepo.findCustomerById(id).isPresent();
       org.junit.jupiter.api.Assertions.assertFalse(notExistAfterDelete);

   }

   @Test
   @Order(4)
   public void findCustomerByIdExistTest(){
       Customer customer = new Customer();
       customer.setId(1l);
       customer.setFirstName("amine");
       customer.setLastName("benseghir");
       customer.setEmail("aa@aa.aa");
       customer.setPhone("06789634");
       customer.setBirthday("12-05-1990");
       customer.setAddress("beijing china");
       customerRepo.save(customer);
      customer = customerRepo.findCustomerById(1l).orElse(customer);
       Assertions.assertThat(customer.getId()).isEqualTo(1l);
   }
   @Test
   @Order(5)
    public void findCustomerByIdNotExistTest(){
        Customer customer = new Customer();
        customer.setId(3l);
        customer.setFirstName("amine");
        customer.setLastName("benseghir");
        customer.setEmail("aa@aa.aa");
        customer.setPhone("06789634");
        customer.setBirthday("12-05-1990");
        customer.setAddress("beijing china");
        customerRepo.save(customer);
        customer = customerRepo.findCustomerById(1l).orElse(customer);
        Assertions.assertThat(customer.getId()).isNotEqualTo(1l);
    }

}
